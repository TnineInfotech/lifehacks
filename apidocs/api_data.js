define({ "api": [
  {
    "type": "post",
    "url": "/tags",
    "title": "Add/Remove Tag",
    "name": "AddRemove_Tag",
    "group": "Admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;add&quot;,&quot;remove&quot;]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>eg:'cars'.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>Status 200.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/admin.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/admin/approval/",
    "title": "Approve a hack.",
    "name": "ApproveHack",
    "group": "Admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;approve&quot;,&quot;remove&quot;]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Any comment eg: reason for rejection</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>The approved hack.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/admin.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/authenticate/user",
    "title": "Authenticate a user.",
    "name": "AuthenticateUser",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ftoken",
            "description": "<p>Firebase Token Value</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Dummy Token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>{success,token}.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/authenticate.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/category/new",
    "title": "Add a category.",
    "name": "AddCategory",
    "group": "Categories",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Name of category</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Id of the added category.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Categories"
  },
  {
    "type": "post",
    "url": "/category/subcat/new",
    "title": "Add a subcategory.",
    "name": "AddSubCategory",
    "group": "Categories",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Name of subcategory</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>Id of the added subcategory.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Categories"
  },
  {
    "type": "get",
    "url": "/category/all",
    "title": "Get all category.",
    "name": "AllCategory",
    "group": "Categories",
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Categories"
  },
  {
    "type": "post",
    "url": "/image/save",
    "title": "Save an image.",
    "name": "AddImage",
    "group": "Files",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>Base64 encoded image</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>status and url of the image.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/image.js",
    "groupTitle": "Files"
  },
  {
    "type": "post",
    "url": "/contents/submit",
    "title": "Add a new hack",
    "name": "AddHack",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of Hack.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Category title of Hack.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category_id",
            "description": "<p>Id of the category.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "body",
            "description": "<p>Body of Hack.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "external_url",
            "description": "<p>External Url(3rd party).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "internal_url",
            "description": "<p>Internal Url(Inside App redirection).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "author",
            "description": "<p>Name of author.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "images",
            "description": "<p>A collection of image urls.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "videos",
            "description": "<p>A collection of video urls.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tags",
            "description": "<p>Tagof Hack.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "approved",
            "description": "<p>Approve status of Hack.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "serial_number",
            "description": "<p>Serial Number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_type",
            "description": "<p>Type of Hack.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "saved",
            "description": "<p>Response.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/contents/special",
    "title": "Add a special hack.",
    "name": "AddSpecialHack",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "event",
            "description": "<p>[&quot;day&quot;,&quot;month&quot;,&quot;year&quot;,&quot;editors_choice&quot;,&quot;banner&quot;]</p>"
          },
          {
            "group": "Parameter",
            "type": "JSON",
            "optional": false,
            "field": "ui",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Special Hack Document.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/contents/bookmark/all",
    "title": "Get all bookmarks.",
    "name": "AllBookmarkHacks",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "defaultValue": "5",
            "description": "<p>Limit Hack Count.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "skip",
            "defaultValue": "0",
            "description": "<p>Will skip the first &quot;n&quot; hacks.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>All trending hacks.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/contents/upvotes/all",
    "title": "Get trending hacks.",
    "name": "AllUpvotesHacks",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serial_number",
            "description": "<p>Serial Number</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "skip",
            "defaultValue": "0",
            "description": "<p>n records will be skipped.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>All trending hacks.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/contents/edit",
    "title": "Edit a hack.",
    "name": "EditHack",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Mimics",
            "optional": false,
            "field": "Params",
            "description": "<p>Similar to Adding Hack. Same param names</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>Status 200.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "get",
    "url": "/contents/special/:event",
    "title": "Add a special hack.",
    "name": "GetSpecialHack",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "event",
            "description": "<p>[&quot;day&quot;,&quot;month&quot;,&quot;year&quot;,&quot;editors_choice&quot;,&quot;banner&quot;]</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Special Hack Document.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/special_hacks.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/contents/retrieve",
    "title": "Retrieve hacks.",
    "name": "RetrieveHack",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>Count of hacks to retrieve.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "category_id",
            "description": "<p>Limit hacks by category_id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Timestamp",
            "optional": true,
            "field": "timestamp",
            "description": "<p>All hacks created after the timestamp.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "serial_number",
            "description": "<p>All hacks having greater serial number than the sent value.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "tag",
            "description": "<p>Single tag value , eg:cars.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>All matching records.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/contents/trending",
    "title": "Get trending hacks.",
    "name": "TrendingHacks",
    "group": "Hacks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "days",
            "defaultValue": "20",
            "description": "<p>Trending Period</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "defaultValue": "5",
            "description": "<p>Limit Hack Count.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>All trending hacks.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Hacks"
  },
  {
    "type": "post",
    "url": "/users/log",
    "title": "Log a user.",
    "name": "LogUser",
    "group": "LogUser",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "type",
            "defaultValue": "view",
            "description": "<p>eg:&quot;view&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "hack_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>{success,token}.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "LogUser"
  },
  {
    "type": "post",
    "url": "/partners/add",
    "title": "Add a",
    "name": "AddPartner",
    "group": "Partners",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of partner</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User id of the user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Partner Document.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/partners.js",
    "groupTitle": "Partners"
  },
  {
    "type": "GET",
    "url": "/partners/bulk",
    "title": "Bulk upload partners",
    "name": "BulkPartners",
    "group": "Partners",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;add&quot;,&quot;remove&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>Status 200.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/partners.js",
    "groupTitle": "Partners"
  },
  {
    "type": "post",
    "url": "/partners/all",
    "title": "Get all partners with status active.",
    "name": "GetPartners",
    "group": "Partners",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Partner Document.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/partners.js",
    "groupTitle": "Partners"
  },
  {
    "type": "post",
    "url": "/report/new",
    "title": "Add a report type.",
    "name": "AddReportType",
    "group": "Reporting",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title of report type</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Added Report.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/reporting.js",
    "groupTitle": "Reporting"
  },
  {
    "type": "get",
    "url": "/report/reasons",
    "title": "Get all report type.",
    "name": "GetReportTypes",
    "group": "Reporting",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Report Types.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/reporting.js",
    "groupTitle": "Reporting"
  },
  {
    "type": "post",
    "url": "/report/hack",
    "title": "Report a hack.",
    "name": "ReportHack",
    "group": "Reporting",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "report_id",
            "description": "<p>ID of report</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;add&quot;,&quot;remove&quot;]</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_",
            "description": "<p>&quot;ok&quot;   Status 200.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/reporting.js",
    "groupTitle": "Reporting"
  },
  {
    "type": "post",
    "url": "/tags/add",
    "title": "Add a tag.",
    "name": "AddTag",
    "group": "Tags",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tag",
            "description": "<p>Title of tag</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Tag Document.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/tags.js",
    "groupTitle": "Tags"
  },
  {
    "type": "post",
    "url": "/contents/tags/all",
    "title": "Get all bookmarks.",
    "name": "AllTags",
    "group": "Tags",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "JSON",
            "description": "<p>All trending hacks.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "Tags"
  },
  {
    "type": "GET",
    "url": "/tags/bulk",
    "title": "Bulk upload tags",
    "name": "BulkTags",
    "group": "Tags",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;add&quot;,&quot;remove&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>Status 200.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/tags.js",
    "groupTitle": "Tags"
  },
  {
    "type": "get",
    "url": "/tags/all",
    "title": "Get all tags with status active.",
    "name": "GetTags",
    "group": "Tags",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>Tag Document.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/tags.js",
    "groupTitle": "Tags"
  },
  {
    "type": "put",
    "url": "/contents/bookmark/:action/:hack_id",
    "title": "Add/Remove a  Bookmark",
    "name": "DoBookmark",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;add&quot;,&quot;remove&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>Status 200.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/contents/bookmarks",
    "title": "Request all Bookmarks",
    "name": "GetBookmarks",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "bookmarks.",
            "description": "<p>All bookmark records.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/contents/upvotes",
    "title": "Request all Upvotes",
    "name": "GetUpvotes",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "bookmarks.",
            "description": "<p>All bookmark records.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/sync",
    "title": "Sync a user.",
    "name": "SyncUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old_user_id",
            "description": "<p>The user id from app</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_user_id",
            "description": "<p>The user id from response</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>{success,token}.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/update",
    "title": "Update a user.",
    "name": "UpdateUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "type",
            "defaultValue": "view",
            "description": "<p>eg:&quot;view&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "hack_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "_JSON",
            "description": "<p>{success,token}.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/contents/upvote/:action/:hack_id",
    "title": "Upvote a hack.",
    "name": "UpvoteHack",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>[&quot;add&quot;,&quot;remove&quot;]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hack_id",
            "description": "<p>Hack Id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ok",
            "description": "<p>Status 200.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "Error",
            "description": "<p>Error log.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/contents.js",
    "groupTitle": "User"
  }
] });
