var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Hacks = require('./../models/hacks');
var Bookmarks = require('./../models/bookmarks');
var Upvotes = require('./../models/upvotes');
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
var async = require('async');
var multer = require('multer');
var upload = multer({dest: 'bin/'});
var Cat = require('./../models/categories');
var bulk_upload = require('./bulkupload');

router.get("/hack/add",function(req,res){
	res.render("add")
});

router.get("/hack/edit/:id",function(req,res){
	id=req.params.id;
	id=new ObjectId(id);
	console.log(id)
	Hacks.findOne({_id:id},(err,h)=>{
		console.log("err",err);
  		console.log("HHH",h);
		res.render("edit",{h:JSON.stringify(h)})
	})
	
});

router.get("/reports",function(req,res){
	res.render("reports")
})
module.exports = router;