var express = require('express');
var mongoose=require('mongoose');
var router = express.Router();
var Hacks=require('./../models/hacks');
var Tags=require('./../models/tags');
var ObjectId = require('mongoose').Types.ObjectId; 
var Auth =require('./../middlewares/auth');
router.use("*",Auth.auth);

/**
 * @api {post} /tags/add Add a tag.
 * @apiName AddTag
 * @apiGroup Tags 
 *
 * @apiParam {String} tag Title of tag
 * @apiSuccess {Array} _JSON   Tag Document.
 * @apiError {String} Error  Error log.
 */
router.post("/add",function(req,res){
		params=req.body;
		user_id=new ObjectId(req.user_id);
		const $data={
			"tag":params.tag,
			"user_id":user_id
		}
		Tags.findOrCreate($data,function(err,cat){
			if (err) {
				res.json({status:false,short_message:err.message});
			}
			res.json(cat);
		});
});

/**
 * @api {get} /tags/all Get all tags with status active.
 * @apiName GetTags
 * @apiGroup Tags 
 *
 * @apiSuccess {Array} _JSON   Tag Document.
 * @apiError {String} Error  Error log.
 */
router.get("/all",function(req,res){
		Tags.find({"status":"active"},function(err,tag){
				res.json(tag);

		});

})

/**
 * @api {GET} /tags/bulk Bulk upload tags
 * @apiName BulkTags
 * @apiGroup Tags
 *
 * @apiParam {String} action ["add","remove"].
 * @apiParam {String} hack_id Hack Id
 *
 * @apiSuccess {String} ok  Status 200.
 * 
 */

 router.get("/bulk",function(req,res){
    params=req.body;
	user_id=new ObjectId(req.user_id);
    
    json=require("./tags.json");
        for (var i = json.length - 1; i >= 0; i--) {
            tag=json[i].tag;
            const $data={
			"tag":tag,
			"user_id":user_id
			}

			Tags.findOrCreate($data,function(err,cat){
			if (err) {
				console.log({status:false,short_message:err.message});
			}
			console.log(tag);
			});


        }

    res.send("done")
 })
module.exports = router;
