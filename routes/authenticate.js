var express = require('express');
var router = express.Router();
var User=require('./../models/users');
var jwt    = require('jsonwebtoken');
var GoogleAuth = require('google-auth-library');
var auth = new GoogleAuth;
var Config=require("./../config");
var app = express();
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
app.set('token_secret', Config.secret);




/**
 * @api {post} /authenticate/user Authenticate a user.
 * @apiName AuthenticateUser
 * @apiGroup Authentication 
 *
 * @apiParam {String} name Name of user
 * @apiParam {String} email Email of user
 * @apiParam {String} ftoken Firebase Token Value
 * @apiParam {String} access_token Dummy Token
 * @apiSuccess {Array} _JSON   {success,token}.
 * @apiError {String} Error  Error log.
 */


router.post("/user",function(req,res){
	/*Google Backend Verification*/
	const params=req.body;
	$tobject={
		
		email:params.email

		};
	$data={
		name:params.name,
		auth_provider:"google",
		email:params.email,
		ftoken:params.ftoken

		};
	console.log($tobject);
	User.findOrCreate($tobject,function(err,user){
		if (err) {
                        console.log(err);
			return res.sendStatus(403).send({status:false});
		}
		$tobject['_id']=user._id;
		var token = jwt.sign($tobject,app.get('token_secret'), {
      		
    		});
		User.findOneAndUpdate($tobject,{ $set:$data},{$new:true},function(err,user){
				res.json({
		          success: true,
		          token: token,
		          user:user
		        });
		});
		
		
	});
});


module.exports = router;