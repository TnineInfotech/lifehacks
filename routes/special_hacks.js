var express = require('express');
var mongoose=require('mongoose');
var router = express.Router();
var Hacks=require('./../models/hacks');
var SpecialHack=require('./../models/special_hacks');
var ObjectId = require('mongoose').Types.ObjectId; 
var Auth =require('./../middlewares/auth');



/**
 * @api {get} /contents/special/:event Add a special hack.
 * @apiName GetSpecialHack
 * @apiGroup Hacks 
 *
 * @apiParam {String} event ["day","month","year","editors_choice","banner"]
 * @apiSuccess {Array} _JSON   Special Hack Document.
 * @apiError {String} Error  Error log.
 */
router.get("/special/:event",function(req,res){
	const params=req.params;
	const event=params.event;
	let data;
	$limit=99;
	now = new Date;
	if (event=="day") {
		date=new Date(now.getFullYear(), now.getMonth(), now.getDate());
		$limit=1;

	}


	SpecialHack.aggregate(
		 {$match: {'status': 'active','event':event}},
		 {$lookup: {from: 'hacks', localField: 'hack_id', foreignField: '_id', as: 'hacks'} }
		).sort("-created_at").limit($limit).exec(function(err,sh){
			if (err) {
				console.log(err);
				res.sendStatus(500)
			}else{

				res.json(sh);
			}
		});

})



module.exports = router;
