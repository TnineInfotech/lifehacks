var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Hacks = require('./../models/hacks');
var Bookmarks = require('./../models/bookmarks');
var Upvotes = require('./../models/upvotes');
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
var async = require('async');
var multer = require('multer');
var upload = multer({dest: 'bin/'});
var Cat = require('./../models/categories');
var bulk_upload = require('./bulkupload');
var Promise = require('bluebird');
router.use("*", Auth.auth);


/**
 * @api {post} /admin/approval/ Approve a hack.
 * @apiName ApproveHack
 * @apiGroup Admin
 *
 * @apiParam {String} action ["approve","remove"] 
 * @apiParam {String} hack_id Hack Id 
 *@apiParam {String} comment Any comment eg: reason for rejection
 * @apiSuccess {Array} JSON  The approved hack.
 * @apiError {String} Error  Error log.
 */
router.post("/approval", function (req, res) {
    params = req.body;
    if (params.action == "approve") {
        var $process = {'approved': true}
    } else if (params.action == "remove") {
        var $process = {'approved': false};
    } else {
        res.json(res.json({"status": "failure", "short_message": "Invalid Action"}));

    }
    if (params.comment != null && params.comment!=undefined) {
    	$process['comment']=params.comment;
    }
    hack_id = new ObjectId(params.hack_id);
    //
    try {

        Hacks.findOneAndUpdate({_id: hack_id, deleted_at: null, hidden: false, deleted: false},
            {$set: $process}, {new: true}, function (err, doc) {
                /*Handler*/
                if (err) {
                    console.log(err);
                    res.json({"status": "failure", "short_message": "ServerError#Contents#001"})
                }
                else if (!doc) {
                    res.json({"status": "failure", "short_message": "No Matching Record"})
                }
                else {
                    res.json({"approved": doc.approved});
                }

            });

    }
    catch (err) {
        res.json({"status": "failure", "short_message": "ServerError#Contents#002"})
    }


});


/**
 * @api {post} /tags Add/Remove Tag
 * @apiName AddRemove Tag
 * @apiGroup Admin
 *

 * @apiParam {String} action ["add","remove"]  
 * @apiParam {String} tag eg:'cars'. 
 * 
 * @apiSuccess {String} ok  Status 200.
 * @apiError {String} Error  Error log.
 */
 router.put("/tags", function (req, res) {

    const params = req.body;
    var ret = "failure";
    var action = 0;
    if (params.action == "add") {
        to_update = {status: 'active'};
        action = 1;
    } else if (params.action == "remove") {
        to_update = {status: 'removed'}
        action = -1;
    } else {
        res.json(ret);

    }

    hack_id = new ObjectId(params.hack_id);
    user_id = new ObjectId(req.user_id);
    //
    try {

        Hacks.findOne({
            _id: hack_id,
            deleted_at: null,
            approved: true,
            hidden: false,
            deleted: false
        }, function (err, hack) {
            if (err) {
                res.sendStatus(500);
            }
            else if (hack != null) {

                Upvotes.update({user_id: user_id, hack_id: hack_id}, {$set: to_update}, {
                    upsert: true,
                    setDefaultsOnInsert: true
                }, function (err, upvote) {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log(upvote);
                        if (upvote.nModified == 1 || upvote.upserted != undefined) {
                            if (params.action == "remove") {
                                hack["meta"]["upvotes"] = hack["meta"]["upvotes"] - 1;
                            } else {
                                hack["meta"]["upvotes"] = hack["meta"]["upvotes"] + 1;
                            }
                        }

                        hack.save(function (err) {
                            console.log(err);
                            console.log("error in saving");
                        });
                        res.sendStatus(200);
                    }

                });


            } else {
                res.sendStatus(404);
            }
        });

    }
    catch (err) {
        res.json({"status": "failure", "short_message": "ServerError#Contents#002"})
    }


});
module.exports = router;
