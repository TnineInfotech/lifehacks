var express = require('express');
var mongoose=require('mongoose');
var router = express.Router();
var Hacks=require('./../models/hacks');
var Partners=require('./../models/partners');
var ObjectId = require('mongoose').Types.ObjectId; 
var Auth =require('./../middlewares/auth');
router.use("*",Auth.auth);

/**
 * @api {post} /partners/add Add a 
 * @apiName AddPartner
 * @apiGroup Partners 
 *
 * @apiParam {String} name Name of partner
 *@apiParam {String} user_id User id of the user
 * @apiSuccess {Array} _JSON   Partner Document.
 * @apiError {String} Error  Error log.
 */
router.post("/add",function(req,res){
		params=req.body;
		user_id=new ObjectId(params.user_id);
		const $data={
			"name":params.name,
			"user_id":user_id
		}
		Partners.findOrCreate($data,function(err,cat){
			if (err) {
				res.json({status:false,short_message:err.message});
			}
			res.json(cat);
		});
});

/**
 * @api {post} /partners/all Get all partners with status active.
 * @apiName GetPartners
 * @apiGroup Partners 
 *
 * @apiSuccess {Array} _JSON   Partner Document.
 * @apiError {String} Error  Error log.
 */
router.post("/all",function(req,res){
		Partners.find({},function(err,Partner){
				res.json(Partner);

		});

})

/**
 * @api {GET} /partners/bulk Bulk upload partners
 * @apiName BulkPartners
 * @apiGroup Partners
 *
 * @apiParam {String} action ["add","remove"].
 * @apiParam {String} hack_id Hack Id
 *
 * @apiSuccess {String} ok  Status 200.
 * 
 */

 router.get("/bulk",function(req,res){
    params=req.body;
	user_id=new ObjectId(params.user_id);
    
    json=require("./partners.json");
        for (var i = json.length - 1; i >= 0; i--) {
            Partner=json[i].Partner;
            const $data={
			"Partner":Partner,
			"user_id":user_id
			}

			Partners.findOrCreate($data,function(err,cat){
			if (err) {
				console.log({status:false,short_message:err.message});
			}
			console.log(Partner);
			});


        }

    res.send("done")
 })
module.exports = router;
