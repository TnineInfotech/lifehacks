var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Hacks = require('./../models/hacks');
var Tags = require('./../models/tags');
var Bookmarks = require('./../models/bookmarks');
var Reports = require('./../models/bookmarks');
var Upvotes = require('./../models/upvotes');
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
var async = require('async');
var multer = require('multer');
var upload = multer({dest: 'bin/'});
var Cat = require('./../models/categories');
var bulk_upload = require('./bulkupload');
var SpecialHack=require('./../models/special_hacks');
var Promise = require('bluebird');

var app = express();

router.use("*", Auth.auth);
/**
 * @api {post} /users/log Log a user.
 * @apiName LogUser
 * @apiGroup LogUser 
 * @apiParam {string} [type=view] eg:"view"
 * @apiParam {string} hack_id 
 * @apiSuccess {Array} _JSON   {success,token}.
 * @apiError {String} Error  Error log.
 */
router.post("/log",function(req,res){
	/*Google Backend Verification*/
	const params=req.body;
			
	hack_id=new ObjectId(params.hack_id);
	user_id=new ObjectId(req.user_id);
	$data={
		
		email:params.email
	

		};
	console.log($data);
	User.findOrCreate($data,function(err,user){
		if (err) {
			res.sendStatus(403).send({status:false});
		}

		console.log(user);
		var token = jwt.sign(user.toObject(),app.get('token_secret'), {
      		
    		});
		console.log(token);
		res.json({
          success: true,
          token: token,
          user:user
        });
		
	});
});

/**
 * @api {post} /users/update Update a user.
 * @apiName UpdateUser
 * @apiGroup User 
 * @apiParam {string} [type=view] eg:"view"
 * @apiParam {string} hack_id 
 * @apiSuccess {Array} _JSON   {success,token}.
 * @apiError {String} Error  Error log.
 */

 router.post("/update",function(req,res){

 	const params=req.body;
 	$data={
 		email:params.email
 	}
 	console.log("decoded",req.decoded)
 	console.log("$data",$data)
 	user_old_email=req.decoded.email;
 	console.log("user_old_email",user_old_email)
 	 User.findOneAndUpdate({email:user_old_email}, {$set:$data}, function (err, user) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        } else {
        	console.log(user)
         	 res.send(user);


        }

    });
 })



/**
 * @api {post} /users/sync Sync a user.
 * @apiName SyncUser
 * @apiGroup User
 * @apiParam {String} old_user_id The user id from app
 * @apiParam {String} new_user_id The user id from response

 * @apiSuccess {Array} _JSON   {success,token}.
 * @apiError {String} Error  Error log.
 */
 router.post("/sync",function(req,res){
 	const params=req.body;
 	old_user_id=new ObjectId(params.old_user_id)
 	new_user_id=new ObjectId(params.new_user_id)
 	Hacks.update({user_id:old_user_id}, {user_id:new_user_id}, {multi: true}, 
	    function(err, num) {
	    	console.log("Hacks Sync",err);
	        /*UPDATE BOOKMARKS*/
	        Bookmarks.update({user_id:old_user_id},{user_id:new_user_id},{multi:true},
	        	function(err1,num1){
	        		console.log("Bookmarks Syncs",err1);
	        		Upvotes.update({user_id:old_user_id},{user_id:new_user_id},{multi:true},
	        			function(err2,num2){
	        				console.log("Upvotes Sync",err2);
	        				Reports.update({user_id:old_user_id},{user_id:new_user_id},{multi:true},
	        				function(err3,num3){
	        					console.log("Report Sync",err3)
	        					res.sendStatus(200)
	        				})
	        			})
	        	}) 
	    }
	)
 })



module.exports = router;
