var express = require('express');
var router = express.Router();
var Cat=require('./../models/categories');
var Subcat=require('./../models/subcategories');
const findOrCreate = require('mongoose-find-or-create');

/**
 * @api {post} /category/new Add a category.
 * @apiName AddCategory
 * @apiGroup Categories
 *

 * @apiParam {String} title Name of category  

 *
 * @apiSuccess {String} _id  Id of the added category.
 * @apiError {String} Error  Error log.
 */
router.post('/new',function(req,res){
	const params=req.body;
	const $data={
		title:params.title
	};
	
	Cat.findOrCreate($data,function(err,cat){
		if (err) {
			res.json({status:false,short_message:err.message});
		}
		res.json(cat);
	});
});
router.post('/cat/id',function(req,res){
	const params=req.body;
	Cat.findOne({title:params.title},function(err,cat){
		if (err) {console.log(err); res.send("err")}else{
			if(cat!=null ){
				res.send(cat._id);
			}else{
				res.send("err")
			}
		}
		
	});
}),


/**
 * @api {get} /category/all Get all category.
 * @apiName AllCategory
 * @apiGroup Categories
 * 
 *

 */
router.post('/all',function(req,res){

	
	Cat.find({deleted:false},function(err,cat){
		res.send(cat)
	
	});
});
router.post('/cat/id',function(req,res){
	const params=req.body;
	Cat.findOne({title:params.title},function(err,cat){
		if (err) {console.log(err); res.send("err")}else{
			if(cat!=null ){
				res.send(cat._id);
			}else{
				res.send("err")
			}
		}
		
	});
}),

/**
 * @api {post} /category/subcat/new Add a subcategory.
 * @apiName AddSubCategory
 * @apiGroup Categories
 *

 * @apiParam {String} title Name of subcategory  

 *
 * @apiSuccess {String} _id  Id of the added subcategory.
 * @apiError {String} Error  Error log.
 */
router.post('/subcat/new',function(req,res,next){
	const params=req.body;
	const $data={
		title:params.title
	};
	
	Subcat.findOrCreate($data,function(err,subcat){
		if (err) {
			res.json({status:false,short_message:err.message});
		}
		res.json(subcat);
	});
}); 

module.exports = router;