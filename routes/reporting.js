var express = require('express');
var mongoose=require('mongoose');
var router = express.Router();
var Hacks=require('./../models/hacks');
var Reported=require('./../models/reported_hack');
var Report=require('./../models/reports');
var ObjectId = require('mongoose').Types.ObjectId; 
var Auth =require('./../middlewares/auth');
router.use("*",Auth.auth);
/*/report*/ 
/*Add a new report*/
/**
 * @api {post} /report/new Add a report type.
 * @apiName AddReportType
 * @apiGroup Reporting
 *

 * @apiParam {String} title Title of report type  

 *
 * @apiSuccess {Array} _JSON  Added Report.
 * @apiError {String} Error  Error log.
 */
router.post('/new',function(req,res){
	const params=req.body;
	const $data={
		title:params.title
	};
	
	Report.findOrCreate($data,function(err,cat){
		if (err) {
			res.json({status:false,short_message:err.message});
		}
		res.json(cat);
	});
});
/*Get all reporting reasons*/

/**
 * @api {get} /report/reasons Get all report type.
 * @apiName GetReportTypes
 * @apiGroup Reporting 
 *
 * @apiSuccess {Array} _JSON   Report Types.
 * @apiError {String} Error  Error log.
 */
router.get('/reasons',function(req,res){
	Report.find({'status':'active'},function(err,r){
		if (err) {
			res.json(500);
		}else{
			res.json(r);
		}
	});
});

/**
 * @api {post} /report/hack Report a hack.
 * @apiName ReportHack
 * @apiGroup Reporting 
 *
 * @apiParam {String} report_id ID of report
 * @apiParam {String} hack_id Hack Id
 * @apiParam {String} action ["add","remove"]
 * @apiSuccess {String} _"ok"   Status 200.
 * @apiError {String} Error  Error log.
 */


router.post("/hack",function(req,res){

	params=req.body;

	hack_id=new ObjectId(params.hack_id);
	user_id=new ObjectId(req.user_id);
	report_id=new ObjectId(params.report_id);
	if (params.action=="add") {
		 to_update={status:'active'};
		action=1;
	}else if(params.action=="remove"){
		to_update={status:'removed'}
		action=-1;
	}else{
		res.json("undefined action");

	}
	try{
		
		Hacks.findOne({_id:hack_id,deleted_at:null,approved:true,hidden:false,deleted:false},function(err,hack){
			if (err) {res.sendStatus(500);}
			else if(hack!=null){
				
				Reported.update({user_id:user_id,hack_id:hack_id,report_id:report_id},{$set:to_update},{upsert: true, setDefaultsOnInsert: true},function(err,upvote){
					if (err) {
						console.log(err)
					}else{	
						console.log(upvote);
						if (upvote.nModified==1 || upvote.upserted != undefined) {
							if (params.action=="remove") {
								hack["meta"]["upvotes"]=hack["meta"]["upvotes"]-1;
							}else{
								hack["meta"]["upvotes"]=hack["meta"]["upvotes"]+1;
							}
						}
						
						hack.save(function(err){
							console.log(err);
							console.log("error in saving");
						});
						res.sendStatus(200);
					}

				});

				

			}else{
				res.sendStatus(404);
			}
		});

	}
	catch(err){
		res.json({"status":"failure","short_message":"ServerError#Contents#002"})
	}
});
/* get all reported hacks*/ 
router.post("/all",function(req,res){
	limit=100
	    Reported.aggregate(
        
            {$match: {'status': 'active'}},
            {$lookup: {from: 'hacks', localField: 'hack_id', foreignField: '_id', as: 'hacks'} },
            {$group: {_id: '$hack_id', reports: {$sum: 1},"hack": {$first: "$hacks"}}},
	    { $project : { _id: 1, reports : 1 ,hack : 1,created_at:1 }} 
        
    ).sort("-reports").limit(limit).exec(function (err,rec){
       // console.log(rec[0]._id);
       

        res.send(rec);
    });
});

module.exports = router;
