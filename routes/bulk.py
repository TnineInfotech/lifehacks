import requests
import json
file_directory="hacks.json"
json_data=open(file_directory).read();
url="http://localhost:3000/contents/submit"
id_url="http://localhost:3000/category/cat/id"
datas=json.loads(json_data);
headers={
	'x-access-token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OWMyM2ZhMTdkZTUxNjQxYWM5OTdkODgiLCJuYW1lIjoiQWJoaXNoZWsgRHViZXkiLCJhdXRoX3Byb3ZpZGVyIjoiZ29vZ2xlIiwiYWNjZXNzX3Rva2VuIjoiMTI0NTUiLCJlbWFpbCI6ImFiaGlzaGVrZHViZXkzMzFAZ21haWwuY29tIiwiX192IjowLCJsYW5ndWFnZSI6ImVuZ2xpc2giLCJyZW1lbWJlcl9tZSI6dHJ1ZSwiYWRtaW4iOmZhbHNlLCJpYXQiOjE1MDYxNjcyNTV9.sgKBIvppG8Gctcs-v2Uzol06aK2Hm2BEp4YOaD8oPIc'}
for data in datas:
	# Get ID
	
	r=requests.post(id_url, data = {'title':data['category']},headers=headers)
	if r.text!="err":
	# print(r.text)
		to_save={
			'title':data['title'],
			'category':data['category'],
			'category_id':r.text,
			'user':'System',
			'serial_number':data['serial_no'],
			'hack_type':data['hack_type'],
			'tags':data['tags'],
			'body':data['body'],
			'external_url':data['external_url'],
			'internal_url':data['internal_url'],

			'images':data['images']
		}
		r=requests.post(url, data =to_save,headers=headers)
		print r.text
	else:
		print "Hack ID not found for "+data['category']
	pass