/*All old routes*/

var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Hack = require('./../models/oldhacks');
var Tags = require('./../models/tags');
var Bookmarks = require('./../models/bookmarks');
var Upvotes = require('./../models/upvotes');
var User = require('./../models/users')
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
var async = require('async');
var multer = require('multer');
var upload = multer({dest: 'bin/'});
var Cat = require('./../models/categories');
var bulk_upload = require('./bulkupload');
var SpecialHack=require('./../models/special_hacks');
var zlib=require("zlib");
var Promise = require('bluebird');



router.post('/getHack',(req,res) =>{

    Hack.find({"serial_no": {$gt : req.body.serial_no}})
        .exec()
        .then((hacks) => {
        	console.log(hacks)
            if (!hacks) {
                return res.status(404).json({
                    status: 400,
                    message: "Hack not found"
                });
            }
            else {
                if(hacks.length>0) {
                    res.json({
                        status: 200,
                        message: "Hacks found",
                        hack_details: hacks
                    });
                }
                else {
                    res.json({
                        status: 200,
                        message: "No recent hacks found"
                    });
                }

            }
     });

});

module.exports = router;