var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Hacks = require('./../models/hacks');
var Tags = require('./../models/tags');
var Bookmarks = require('./../models/bookmarks');
var Upvotes = require('./../models/upvotes');
var User = require('./../models/users')
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
var async = require('async');
var multer = require('multer');
var upload = multer({dest: 'bin/'});
var Cat = require('./../models/categories');
var bulk_upload = require('./bulkupload');
var SpecialHack=require('./../models/special_hacks');
var zlib=require("zlib");
var Promise = require('bluebird');
router.use("*", Auth.auth);

// Helper
function splitt(string,delimiter=",") {

    return new Promise((resolve,reject)=>{
        try{
            resolve(string.split(delimiter))
        }
        catch(err){
            ret=[]
            reject(ret)
        }
        
    })
}

/**
 * @api {post} /contents/special Add a special hack.
 * @apiName AddSpecialHack
 * @apiGroup Hacks 
 *
 * @apiParam {String} event ["day","month","year","editors_choice","banner"]
 * @apiParam {JSON} ui
 * @apiParam {String} hack_id Hack Id
 * @apiSuccess {Array} _JSON   Special Hack Document.
 * @apiError {String} Error  Error log.
 */
router.post("/special",function(req,res){
        params=req.body;
        hack_id=new ObjectId(params.hack_id);
        user_id=new ObjectId(req.user_id);

        const $data={
            "event":params.event,
            "ui":params.ui,
            "hack_id":hack_id,
            "user_id":user_id
        }
        console.log($data);
        SpecialHack.findOrCreate($data,function(err,cat){
        if (err) {
            res.json({status:false,short_message:err.message});
        }
        res.json(cat);
    });
});



/**
 * @api {get} /contents/bookmarks Request all Bookmarks
 * @apiName GetBookmarks
 * @apiGroup User
 *
 * 
 *
 * @apiSuccess {Array} bookmarks. All bookmark records.
 * 
 */
router.get('/bookmarks', function (req, res) {
    user_id = new ObjectId(req.user_id);
    Bookmarks.find({user_id: user_id, status: 'active'}, function (err, bookmarks) {
        res.send(bookmarks);
    })
});

/**
 * @api {get} /contents/upvotes Request all Upvotes
 * @apiName GetUpvotes
 * @apiGroup User
 *
 * 
 *
 * @apiSuccess {Array} bookmarks. All bookmark records.
 * 
 */
router.get('/upvotes', function (req, res) {
    user_id = new ObjectId(req.user_id);
    Upvotes.find({user_id: user_id, status: 'active'}, function (err,upvotes) {
        res.send(upvotes);
    })
});
router.get('/bulkUpload', function (req, res) {
    bulk_upload.bulk_upload(req, res);
});

/**
 * @api {put} /contents/bookmark/:action/:hack_id Add/Remove a  Bookmark
 * @apiName DoBookmark
 * @apiGroup User
 *
 * @apiParam {String} action ["add","remove"].
 * @apiParam {String} hack_id Hack Id
 *
 * @apiSuccess {String} ok  Status 200.
 * 
 */
router.put('/bookmark/:action/:hack_id', function (req, res) {
    /* Validation Block*/
    /* */

    const params = req.params;
    var ret = "failure";
    var action = 0;
    if (params.action == "add") {
        to_update = {status: 'active'};
        action = 1;
    } else if (params.action == "remove") {
        to_update = {status: 'removed'}
        action = -1;
    } else {
        res.json(ret);

    }

    hack_id = new ObjectId(params.hack_id);
    user_id = new ObjectId(req.user_id);
    try {
        /*Add record in the Bookmark table for that user. */

        Hacks.findOne({
            _id: hack_id,
            deleted_at: null,
            approved: true,
            hidden: false,
            deleted: false
        }, function (err, hack) {
            if (err) {
                res.sendStatus(500);
            }
            else if (hack != null) {


                Bookmarks.update({user_id: user_id, hack_id: hack_id}, {$set: to_update}, {
                    upsert: true,
                    setDefaultsOnInsert: true
                }, function (err, bookmark) {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log(bookmark);
                        if (bookmark.nModified == 1 || bookmark.upserted != undefined) {
                            if (params.action == "remove") {
                                hack["meta"]["bookmarks"] = hack["meta"]["bookmarks"] - 1;
                            } else {
                                hack["meta"]["bookmarks"] = hack["meta"]["bookmarks"] + 1;
                            }
                            hack.save(function (err) {
                                console.log(err);
                                console.log("error in saving");
                            });
                        }

                        console.log(bookmark);
                        res.sendStatus(200);
                    }

                });


            } else {
                res.sendStatus(404);
            }
        });

    } catch (err) {
        console.log("MAIN ERR");
        console.log(err);
    }
});


/**
 * @api {post} /contents/submit Add a new hack
 * @apiName AddHack
 * @apiGroup Hacks
 *
 * @apiParam {String} title Title of Hack.
 * @apiParam {String} category Category title of Hack. 
 * @apiParam {String} category_id Id of the category. 
 * @apiParam {String} body  Body of Hack.
 * @apiParam {String} [external_url]  External Url(3rd party).
 * @apiParam {String} [internal_url] Internal Url(Inside App redirection).
 * @apiParam {String}  [author]  Name of author.
 * @apiParam {Array} [images] A collection of image urls.
* @apiParam {Array} [videos] A collection of video urls.
 * @apiParam {String} tags Tagof Hack.
 * @apiParam {Boolean} approved Approve status of Hack.
 * @apiParam {Number}  [serial_number] Serial Number.
 * @apiParam {String} hack_type Type of Hack.
 *
 * @apiSuccess {String} saved Response.
 * @apiError {String} Error  Error log.
 */
router.post("/submit", function (req, res) {
    var params = req.body;
    console.log(params);
    /*Sanitize the params and add validations*/
    /*Do User Validation and get user_id*/
    /*Convert params to a kv array*/
    category_id = new ObjectId(params.category_id);
    user_id=new ObjectId(req.user_id);
    if (params.tags!=undefined && params.tags!=null) {
    	params.tags=JSON.parse(params.tags);
    }
    if (params.images!=undefined && params.images!=null) {
    	params.images=JSON.parse(params.images);
    }

    if (params.videos!=undefined && params.videos!=null) {
        try{
                  params.videos=JSON.parse(params.videos);

        }
        catch(err){

        }
    }
    if (params.content_partner!=undefined && params.content_partner!=null &&
        params.content_partner=="true"
        ) {
        
        user_id=new ObjectId(params.partners);
    }
    console.log("HH",user_id);
    var to_save = {
        title: params.title,
        category: params.category,
        category_id: category_id,
        subcategory: params.subcategory,
        body: params.body,
        user_id:user_id,
        external_url: params.external_url,
        internal_url: params.internal_url,
        author: params.author,
        images: params.images,
        videos:params.videos,
        tags: params.tags,
        approved: params.approved,
        serial_number: params.serial_number,
        hack_type: params.hack_type
    };
    if (params.parent_id != null && params.parent_id != undefined) {
        parent_id = new ObjectId(params.parent_id);
        to_save.parent_id = parent_id;
    }
	console.log(to_save)


    User.findOne({_id:user_id},function(e,d){
        console.log("USER",d);
        if (to_save.author=="" || to_save.author==null || to_save.author==undefined) {
            to_save["author"]=d.name;
        }
        console.log("TO SAVE",to_save)
        hack = new Hacks(to_save).save(function (err, doc) {
            console.log("TO SAVE1",to_save)

            if (err) {
            console.log("ERR1");
            console.log(err);
            } else {
            res.send('saved');
            }

            // res.send(doc);
            });
    })
    


    

});
/**
 * @api {post} /contents/retrieve Retrieve hacks.
 * @apiName RetrieveHack
 * @apiGroup Hacks
 *
 * @apiParam {Number} [limit=10] Count of hacks to retrieve.
 * @apiParam {String} [category_id] Limit hacks by category_id. 
 * @apiParam {Timestamp} [timestamp] All hacks created after the timestamp. 
 * @apiParam {Number} [serial_number]  All hacks having greater serial number than the sent value.
 * @apiParam {String} [tag]  Single tag value , eg:cars.


 *
 * @apiSuccess {Array} JSON  All matching records.
 * @apiError {String} Error  Error log.
 */

router.post("/retrieve", function (req, res) {
    /*
    ToDO: Add validation parameters
    */
    // req.setTimeout(0)
    params = req.body;
    limit = 10;
    validator = {
        'approved': true,
        'deleted':false
    };

    console.log("CAT ID",params.category_id);
    if (params.category_id != null && params.category_id != undefined && params.category_id.length > 0) {
        // validator.category_id=ObjectId(params.category_id);
        // params.category_id = JSON.parse(params.category_id);
       
        validator['category_id'] =ObjectId(params.category_id);
    }
    console.log("CAT",params.category);
    if (params.category != null && params.category != undefined && params.category.length > 0) {
        // validator.category_id=ObjectId(params.category_id);
        // params.category_id = JSON.parse(params.category_id);
       
        validator['category'] =params.category;
    }

    if (params.timestamp != null && params.timestamp != undefined) {

        // validator.meta={updated_at:{$gte:new Date(params.timestamp)}};
        validator['meta.updated_at'] = {$gte: new Date(params.timestamp)};

    }

    if (params.serial_number !=null && params.serial_number!=undefined) {
    	validator['serial_number'] = {$gte:params.serial_number};
    }

    if (params.tag!=null && params.tag!=undefined) {
    	validator['tags']=params.tag
    }

    if (params.limit != null && params.limit != undefined) {
        limit = parseInt(params.limit);
    }
    if (params.user_id !=null && params.user_id!=undefined) {
        validator['user_id']=ObjectId(params.user_id);
        delete validator.approved;
    }
    // console.log(limit);
    console.log("validator",validator)
    Hacks.find(validator).limit(limit).exec(function (err, hack) {
        console.log(err);
       
            res.send(hack)
      
        //res.send(hack);
    });

});



/**
 * @api {put} /contents/upvote/:action/:hack_id Upvote a hack.
 * @apiName UpvoteHack
 * @apiGroup User
 *

 * @apiParam {String} action ["add","remove"]  
 * @apiParam {String} hack_id Hack Id. 
 *
 * @apiSuccess {String} ok  Status 200.
 * @apiError {String} Error  Error log.
 */
 router.put("/upvote/:action/:hack_id", function (req, res) {

    const params = req.params;
    var ret = "failure";
    var action = 0;
    if (params.action == "add") {
        to_update = {status: 'active'};
        action = 1;
    } else if (params.action == "remove") {
        to_update = {status: 'removed'}
        action = -1;
    } else {
        res.json(ret);

    }

    hack_id = new ObjectId(params.hack_id);
    user_id = new ObjectId(req.user_id);
    //
    try {

        Hacks.findOne({
            _id: hack_id,
            deleted_at: null,
            approved: true,
            hidden: false,
            deleted: false
        }, function (err, hack) {
            if (err) {
                res.sendStatus(500);
            }
            else if (hack != null) {

                Upvotes.update({user_id: user_id, hack_id: hack_id}, {$set: to_update}, {
                    upsert: true,
                    setDefaultsOnInsert: true
                }, function (err, upvote) {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log(upvote);
                        if (upvote.nModified == 1 || upvote.upserted != undefined) {
                            if (params.action == "remove") {
                                hack["meta"]["upvotes"] = hack["meta"]["upvotes"] - 1;
                            } else {
                                hack["meta"]["upvotes"] = hack["meta"]["upvotes"] + 1;
                            }
                        }

                        hack.save(function (err) {
                            console.log(err);
                            console.log("error in saving");
                        });
                        res.sendStatus(200);
                    }

                });


            } else {
                res.sendStatus(404);
            }
        });

    }
    catch (err) {
        res.json({"status": "failure", "short_message": "ServerError#Contents#002"})
    }


});

/**
 * @api {post} /contents/trending  Get trending hacks.
 * @apiName TrendingHacks
 * @apiGroup Hacks
 *

 * @apiParam {Number} [days=20] Trending Period  
 * @apiParam {Number} [limit=5] Limit Hack Count. 
 *
 * @apiSuccess {Array} JSON All trending hacks.
 * @apiError {String} Error  Error log.
 */

router.post('/trending', function (req, res) {
    params = req.body;

    days = 40;

    limit = 5;
    if (params.days != undefined && params.days != null) {
        days = parseInt(params.days);
    }
    if (params.limit != undefined && params.limit != null) {
        limit = parseInt(params.limit);
    }

    time_val = new Date((new Date().getTime() - (days * 24 * 60 * 60 * 1000)));
    console.log(limit);
    Upvotes.aggregate(
        
            {$match: {'status': 'active', updated_at: {$gte: time_val}}},
            {$lookup: {from: 'hacks', localField: 'hack_id', foreignField: '_id', as: 'hacks'} },
            {$group: {_id: '$hack_id', upvotes: {$sum: 1},"hack": {$first: "$hacks"}}},
	    { $project : { _id: 1, upvotes : 1 ,hack : 1 }} 
        
    ).sort("-upvotes").limit(limit).exec(function (err,rec){
       // console.log(rec[0]._id);
       

        res.send(rec);
    });
});


/**
 * @api {post} /contents/bookmark/all Get all bookmarks.
 * @apiName AllBookmarkHacks
 * @apiGroup Hacks
 *

 * @apiParam {Number} [limit=5] Limit Hack Count. 
 * @apiParam {Number} [skip=0] Will skip the first "n" hacks. 
 * @apiSuccess {Array} JSON All trending hacks.
 * @apiError {String} Error  Error log.
 */

router.post('/bookmarks/all', function (req, res) {
    params = req.body;

    limit = 5;
    skip=0;
    if (params.limit != undefined && params.limit != null) {
        limit = parseInt(params.limit);
    }
    if (params.skip != undefined && params.skip != null) {
        limit = parseInt(params.skip);
    }

    Bookmarks.aggregate(
        
            {$match: {'status': 'active'}},
            {$lookup: {from: 'hacks', localField: 'hack_id', foreignField: '_id', as: 'hacks'} },
            {$group: {_id: '$hack_id', bookmarks: {$sum: 1},"hack": {$first: "$hacks"}}},
            { $project : { _id: 1, bookmarks : 1  }} 
        
    ).sort("-bookmarks").skip(skip).limit(limit).exec(function (err,rec){
       console.log(err);
       
        res.send(rec);
    });
});


/**
 * @api {post} /contents/tags/all Get all bookmarks.
 * @apiName AllTags
 * @apiGroup Tags
 *


 * @apiSuccess {Array} JSON All trending hacks.
 * @apiError {String} Error  Error log.
 */

router.post('/tags/all', function (req, res) {
    Tags.find({},{tag:1,_id:0},(err,tags)=>{
        if (err) {console.log(err)}
        var buf = new Buffer(JSON.stringify(tags), 'utf-8');
        zlib.gzip(buf, function (_, result) {

          // The callback will give you the
                    res.set({
                      'Content-Type': 'application/json',
                      'Content-Encoding': 'gzip'
                      
                    })
                    console.log(result)
                    res.send(result);                     // result, so just send it.
            });
        // res.send(tags);
    });
});


/**
 * @api {post} /contents/upvotes/all  Get trending hacks.
 * @apiName AllUpvotesHacks
 * @apiGroup Hacks
 *

 * @apiParam {Number} serial_number Serial Number  
 * @apiParam {Number} [skip=0] n records will be skipped. 
 *
 * @apiSuccess {Array} JSON All trending hacks.
 * @apiError {String} Error  Error log.
 */

router.post('/upvotes/all', function (req, res) {
    params = req.body;

    skip=0;

    limit = 5;
    if (params.skip != undefined && params.skip != null) {
        skip = parseInt(params.skip);
    }
    if (params.limit != undefined && params.limit != null) {
        limit = parseInt(params.limit);
    }

    
    // console.log(limit);
    Upvotes.aggregate(
        
            {$match: {'status': 'active'}},
            {$lookup: {from: 'hacks', localField: 'hack_id', foreignField: '_id', as: 'hacks'} },
            {$group: {_id: '$hack_id', upvotes: {$sum: 1},"hack": {$first: "$hacks"}}},
        { $project : { _id: 1, upvotes : 1}} 
        
    ).sort("-upvotes").skip(skip).limit(limit).exec(function (err,rec){
       // console.log(rec[0]._id);
       console.log(err);

        res.send(rec);
    });
});



/**
 * @api {post} /contents/edit Edit a hack.
 * @apiName EditHack
 * @apiGroup Hacks
 *

 * @apiParam {String} hack_id Hack Id  
 * @apiParam {Mimics} Params Similar to Adding Hack. Same param names
 *
 * @apiSuccess {String} ok  Status 200.
 * @apiError {String} Error  Error log.
 */
router.post("/edit", function (req, res) {
    params = req.body;
   
    hack_id = new ObjectId(params._id);
    if (params.tags!=null && params.tags!=undefined) {
         params.tags=JSON.parse(params.tags);
    }

    if (params.images!=null || params.images!=undefined || params.images!="") {
         params.images=JSON.parse(params.images);
    }
    if (params.images==null || params.images==undefined || params.images=="") {
       delete params.images;
    }

    // if (params.videos!=null && params.videos!=undefined) {
    //      params.videos=JSON.parse(params.videos);
    // }
     if (params.content_partner!=undefined && params.content_partner!=null &&
        params.content_partner=="true"
        ) {
        
        user_id=new ObjectId(params.partners);
    }

    for (var key in params){
        if (params.key==null && params.key==undefined) {
         delete params.key;
    }

    }
    delete params._id
    User.findOne({_id:user_id},function(e,d){
        console.log("USER",d);
        if (params.author=="" || params.author==null || params.author==undefined) {
            params["author"]=d.name;
        }
        
        Hacks.update({_id: hack_id}, {$set: params},{upsert:true},function (err, hack) {
        if (err) {
            console.log(err);
        } else {
            console.log(hack);
            if (hack.nModified == 1) {
                // hack.markModified("tags");
                res.sendStatus(200);
            } else {

                res.sendStatus(200);
            }


        }

       });
    })

    Hacks.update({_id: hack_id}, {$set: params},{upsert:true},function (err, hack) {
        if (err) {
            console.log(err);
        } else {
            console.log(hack);
            if (hack.nModified == 1) {
                // hack.markModified("tags");
                res.sendStatus(200);
            } else {

                res.sendStatus(200);
            }


        }

    });
});




module.exports = router;
