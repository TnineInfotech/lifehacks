var express = require('express');
var router = express.Router();
var FCM = require('fcm-push');
var Users = require('./../models/users');
var Config = require("./../config");
var ObjectId = require('mongoose').Types.ObjectId;
var Auth = require('./../middlewares/auth');
var https = require('https');
var Promise = require("bluebird");


var async =require("async");

var sleep = require('sleep');
var serverKey = Config.firebase_server_key;

var fcm = new FCM(serverKey);

var message = {
        registration_ids: [],
        data: {
            image:
                "https://about.canva.com/wp-content/uploads/sites/3/2017/02/birthday_banner.png",
            message:
                "Need some free WiFi? The best places to go are Panera, McDonalds, Apple Store, Office Depot, Staples, and Courtyard Marriott.",
            AnotherActivity:
                "True",
            title:
                'New Hacks Are Added',
            onlyUpdate:
                "false"
        }
        /*,
        notification: {
            click_action: 'ACTIVITY_HACK_OF_DAY',
            title: 'New Hacks Are Added',
             body: 'Android device manager>\'Ring\''
        }*/
    }
;

function fcmpush(message, $tlimit) {
    fcm.send(message, function (err, response) {
        sleep.sleep($tlimit)
        if (err) {
            console.log(err);
            console.log("Something has gone wrong!");
            // res.send("Some Error Occurred" + err);
        } else {
            console.log("Successfully sent with response: ", response);

            // res.send("Success" + response);

        }
    }).then(() => {
        console.log("FCM Done")
        // sleep.sleep($tlimit)
        // console.log("pausing")
    });
}

router.get('/all', function (req, res, next) {
    var $climit = 1;
    var $tlimit = 30; //milliseconds

    var $flag = 0;

    Users.find({}).select("ftoken").exec(function (err, users) {
        // users = [
        //     // {ftoken:"eeSSn8007As:APA91bH6G5d6HLFYySC75j88qFx1wKgHRk5fAHsadXOX9ZEoIue0SMvXMb6ELh9Mo-aUanHKrhxrHIIt7j_fGBDwmHeKGSiglYtMelxN4WQ0ZBdACD9YrPdwJWTq_BbDW0rIxWvbUXVy"},

        //     // {ftoken:"eeSSn8007As:APA91bH6G5d6HLFYySC75j88qFx1wKgHRk5fAHsadXOX9ZEoIue0SMvXMb6ELh9Mo-aUanHKrhxrHIIt7j_fGBDwmHeKGSiglYtMelxN4WQ0ZBdACD9YrPdwJWTq_BbDW0rIxWvbUXVy"},

        //     // {ftoken:"eeSSn8007As:APA91bH6G5d6HLFYySC75j88qFx1wKgHRk5fAHsadXOX9ZEoIue0SMvXMb6ELh9Mo-aUanHKrhxrHIIt7j_fGBDwmHeKGSiglYtMelxN4WQ0ZBdACD9YrPdwJWTq_BbDW0rIxWvbUXVy"},

        //     // {ftoken:"eeSSn8007As:APA91bH6G5d6HLFYySC75j88qFx1wKgHRk5fAHsadXOX9ZEoIue0SMvXMb6ELh9Mo-aUanHKrhxrHIIt7j_fGBDwmHeKGSiglYtMelxN4WQ0ZBdACD9YrPdwJWTq_BbDW0rIxWvbUXVy"},

        //     // {ftoken:"eeSSn8007As:APA91bH6G5d6HLFYySC75j88qFx1wKgHRk5fAHsadXOX9ZEoIue0SMvXMb6ELh9Mo-aUanHKrhxrHIIt7j_fGBDwmHeKGSiglYtMelxN4WQ0ZBdACD9YrPdwJWTq_BbDW0rIxWvbUXVy"},
        //     {ftoken: "fVVU9nt5wf4:APA91bGeVsl0Z7DXIm6IQ_FnMGlZGjg9UyWF0qE4tO_xzNtQenGfJs4viD2NgfqpMBM2UEAVcma3KlpH07B--KGCOTFolIJUN0KHY9Ew4hGKgF4iuAOjoWtZp6L77Qdn428ngJz6UmtX"},
        //     {ftoken: "fVVU9nt5wf4:APA91bGeVsl0Z7DXIm6IQ_FnMGlZGjg9UyWF0qE4tO_xzNtQenGfJs4viD2NgfqpMBM2UEAVcma3KlpH07B--KGCOTFolIJUN0KHY9Ew4hGKgF4iuAOjoWtZp6L77Qdn428ngJz6UmtX"},
        //     {ftoken: "fVVU9nt5wf4:APA91bGeVsl0Z7DXIm6IQ_FnMGlZGjg9UyWF0qE4tO_xzNtQenGfJs4viD2NgfqpMBM2UEAVcma3KlpH07B--KGCOTFolIJUN0KHY9Ew4hGKgF4iuAOjoWtZp6L77Qdn428ngJz6UmtX"},
        //     {ftoken: "fVVU9nt5wf4:APA91bGeVsl0Z7DXIm6IQ_FnMGlZGjg9UyWF0qE4tO_xzNtQenGfJs4viD2NgfqpMBM2UEAVcma3KlpH07B--KGCOTFolIJUN0KHY9Ew4hGKgF4iuAOjoWtZp6L77Qdn428ngJz6UmtX"}

        // ]

        async.whilst(function () {

                return $flag <= users.length;

            }
            , function (callback) {

                console.log($flag);
                temp_users = users.slice($flag, ($flag + $climit));
                console.log(temp_users)
                $tokens = temp_users.map((value, index) => value.ftoken);
                console.log($tokens);
                message["registration_ids"] = $tokens;

                fcm.send(message, function (err, response) {
                    sleep.sleep($tlimit)
                    if (err) {
                        console.log(err);
                        console.log("Something has gone wrong!");
                        // res.send("Some Error Occurred" + err);
                    } else {
                        console.log("Successfully sent with response: ", response);

                        // res.send("Success" + response);

                    }
                }).then(() => {
                    console.log("FCM Done")
                    $flag += $climit;
                    setTimeout(function () {
                        callback(null, $flag);
                    }, 1000);
                });

                // r= fcmpush(message,$tlimit);

            },
            function (e, $flag) {
                console.log("ERR", $flag)
            }
        );
        res.send("ok")

    });
});

module.exports = router;
