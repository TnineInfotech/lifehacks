var Hack = require('../models/hacks');
var Promise = require('bluebird');
var jsonfile = require('./hacks2.json');
var fs = require('fs');
var cat = require('../models/categories');
var ObjectId = require('mongoose').Types.ObjectId;


module.exports.bulk_upload = function (req, res) {
    let user_id=new ObjectId("5a3cde7ec7876e120f4757d0")
    return new Promise(function (resolve, reject) {
        //console.log(json)
        try {
            let data_json = new Promise.promisifyAll(jsonfile);
            Promise.each(data_json, function (data) {
                cat.findOne({title:data.category}).exec().then((resp) => {
                    // console.log(resp);
                    if ( resp!==undefined && resp!==null) {
                        //return resp._id;
                        Hack.create({
                            title: data.title,
                            user_id:user_id,
                            category: data.category,
                            category_id: ObjectId(resp._id),
                            serial_number: data.serial_no,
                            hack_type: data.hack_type,
                            tags: data.tags.split(","),
                            body: data.body,
                            external_url: data.external_url,
                            internal_url: data.internal_url,
                            images: data.images,
                            approved: true
                        })
                            .then((savedHack) => {
                                //console.log(savedHack)
                                //resolve(savedHack)
                            }).catch((error) => {
                            //console.log(error);
                            fs.appendFile('error.txt', error+'\r\n', function (err) {
                                if (err) throw err;
                                console.log('Saved to error log!!');
                            })
                        });
                    }else{
                        console.log("Category NOT FOUND",data.category);
                    }
                }).then((response) => {
                    //console.log(response);
                    //console.log(data.category);

                })
            });

        } catch (e) {
            res.json({
                "success": false,
                "Message": "Error inserting hacks!!"
            });
            console.log(e);
        }

    })
};

