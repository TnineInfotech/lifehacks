const mongoose=require('mongoose');

const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

const Schema=mongoose.Schema;

const HackSchema=new Schema({
    title:String,
    category: String,
    category_id: Number,
    parent_id: String,
    subcategory: String,
    body: String,
    external_url: String,
    internal_url: String,
    language: String,
    images: Array,
    tags: Array,
    hack_type: String,
    serial_no: Number

});

HackSchema.plugin(autoIncrement.plugin, 'oldhacks');
const hack =mongoose.model('oldhacks',HackSchema);

module.exports=hack;