var mongoose= require('mongoose');
var Schema=mongoose.Schema;
const findOrCreate = require('mongoose-find-or-create');
var reports=new Schema({
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	title:{type:String,unique:true},
	description:String,
	priority:Number,
	created_at:{type:Date,default:Date.now},
	updated_at:{type:Date,default:Date.now},
	deleted_at:Date,
	deleted:{type:Boolean,default:false},
	status:{
		type:String,
		enum:["active","deleted","removed"],
		default:"active"
	}
});
reports.plugin(findOrCreate);
var Reports = mongoose.model('Reports',reports);
module.exports=Reports;