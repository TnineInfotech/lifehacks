var mongoose= require('mongoose');
var Schema=mongoose.Schema;

var reported_hack=new Schema({
	hack_id:{type:Schema.Types.ObjectId,ref:'hacks'},
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	report_id:{type:Schema.Types.ObjectId,ref:'reports'},
	created_at:{type:Date,default:Date.now},
	updated_at:{type:Date,default:Date.now},
	deleted_at:Date,
	deleted:{type:Boolean,default:false},
	status:{
		type:String,
		enum:["active","deleted","removed"],
		default:"active"
	}
});

var Reported_Hack = mongoose.model('Reported_Hack',reported_hack);
module.exports=Reported_Hack;