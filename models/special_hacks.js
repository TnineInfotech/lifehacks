var mongoose= require('mongoose');
var Schema=mongoose.Schema;
const findOrCreate = require('mongoose-find-or-create');
var special_hacks=new Schema({
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	hack_id:{type:Schema.Types.ObjectId,ref:'hacks'},
	description:String,
	created_at:{type:Date,default:Date.now},
	updated_at:{type:Date,default:Date.now},
	deleted_at:Date,
	deleted:{type:Boolean,default:false},
	ui:{},
	event:{
		type:String,
		enum:["day","month","year","user_favorite","editor_choice","banner"],
		default:"day"
	},
	status:{
		type:String,
		enum:["active","deleted","removed"],
		default:"active"
	},
});
special_hacks.plugin(findOrCreate);
var SpecialHacks = mongoose.model('SpecialHacks',special_hacks);
module.exports=SpecialHacks;