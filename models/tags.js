var mongoose= require('mongoose');
var Schema=mongoose.Schema;
const findOrCreate = require('mongoose-find-or-create');
var tags=new Schema({
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	tag:String,
	description:String,
	created_at:{type:Date,default:Date.now},
	updated_at:{type:Date,default:Date.now},
	deleted_at:Date,
	deleted:{type:Boolean,default:false},
	type:{
		type:String,
		enum:["general","user_favorite","editor_choice","banner"],
		default:"general"
	},
	status:{
		type:String,
		enum:["active","deleted","removed"],
		default:"active"
	},
});
tags.plugin(findOrCreate);
var Tags = mongoose.model('Tags',tags);
module.exports=Tags;