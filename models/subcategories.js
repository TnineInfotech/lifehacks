var mongoose= require('mongoose');
var Schema=mongoose.Schema;
const findOrCreate = require('mongoose-find-or-create');
var subcategories=new Schema({
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	title:String,
	description:String,
	created_at:{type:Date,default:Date.now},
	updated_at:{type:Date,default:Date.now},
	deleted_at:Date,
	deleted:{type:Boolean,default:false},
	status:{
		type:String,
		enum:["active","deleted","removed"],
		default:"active"
	},
});
subcategories.plugin(findOrCreate);
var Subcategories = mongoose.model('Subcategories',subcategories);
module.exports=Subcategories;