var mongoose= require('mongoose');
var Bookmarks=require('./../models/bookmarks');
var Upvote=require('./../models/upvotes');
var autoIncrement = require('mongoose-auto-increment');
var Schema=mongoose.Schema;

var hacks=new Schema({
	title:String,
	parent_id:{type:Schema.Types.ObjectId,ref:'hacks'},
	category:String,
	category_id:{type:Schema.Types.ObjectId,ref:'category'},
	subcategory:String,
	subcategory_id:{type:Schema.Types.ObjectId,ref:'subcategory'},
	body:String,
	serial_number:{type:Number,unique:true},
	tags:[],
	hack_type:String,
	external_url:String,
	internal_url:String,
	language:{type:String,default:'english'},
	demographic:{
		gender:{type:String,default:'all'},
		country:{type:String,default:'global'}
	},
	
	images:[],
	videos:[],
	batch:Number,
	author:String,
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	hidden:{type:Boolean,default:false},
	approved:{type:Boolean,default:true},
	deleted:{type:Boolean,default:false},
	meta:{
		bookmarks:{type:Number,default:0},
		comment:String,
		upvotes:{type:Number,default:0},
		favorites:{type:Number,default:0},
		edits:{type:Number,default:0},
		created_at:{type:Date,default:Date.now},
		updated_at:{type:Date,default:Date.now},
		deleted_at:Date,
		approved_at:Date,
		flagged:{type:Number,default:0}
	}
	

});
autoIncrement.initialize(mongoose.connection);
hacks.plugin(autoIncrement.plugin, { model: 'Hacks', field: 'serial_number' });

/*Update the updated field.*/ 
hacks.pre('save',function(next){
	var updated_at=Date.now();
	this.meta.updated_at=updated_at;
	this.meta.edits+=1;
	next();
});

/*Increment Upvote*/


hacks.methods.upvotes=function(){

}
hacks.methods.bookmarks=function(){
	return 100;
}
var Hacks = mongoose.model('Hacks',hacks);
module.exports=Hacks;