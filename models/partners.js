var mongoose= require('mongoose');
var Schema=mongoose.Schema;
const findOrCreate = require('mongoose-find-or-create');
var partners=new Schema({
	user_id:{type:Schema.Types.ObjectId,ref:'users'},
	name:String,
	created_at:{type:Date,default:Date.now},
	updated_at:{type:Date,default:Date.now},
	deleted_at:Date,
	deleted:{type:Boolean,default:false},
	status:{
		type:String,
		enum:["approved","suspended","pending"],
		default:"approved"
	},
});
partners.plugin(findOrCreate);
var Partners = mongoose.model('Partners',partners);
module.exports=Partners;