const mongoose= require('mongoose');
const Schema=mongoose.Schema;
const findOrCreate = require('mongoose-find-or-create');
const logger=new Schema({
	name:String,
	auth_provider:String,
	email: {
        type:String,
        required: true,
        unique: true
    },
	access_token:{
        type:String,
        required: true,
        unique: true
    },
	admin:{type:Boolean,default:false},
	ftoken:String,
	:dtob:Date,
	remember_me:{type:Boolean,default:true},
	avatar_url:String,
	bio_url:String,
	language:{type:String,default:'english'},
	gender:{type:String}
});
logger.plugin(findOrCreate);

/*Update the updated field.*/ 
logger.pre('save',function(next){
	var updated_at=new Date();
	// this.meta.updated_at=updated_at;
	next();
});


var logger = mongoose.model('logger',logger);
module.exports=logger;