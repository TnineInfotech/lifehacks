var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongoose=require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var index = require('./routes/index');
var users = require('./routes/users');
var contents=require('./routes/contents');
var cats=require('./routes/categories');
var reports=require('./routes/reporting');
var tags=require('./routes/tags');
var partners=require('./routes/partners');
var admin=require('./routes/admin');
var image=require('./routes/image');
var webview=require('./routes/webview');
var notify=require('./routes/firebase');
var legacy=require('./routes/legacy');
var special_hacks=require('./routes/special_hacks.js');
var authenticate=require('./routes/authenticate.js');
var Config=require('./config');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false,limit:'50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'assets')));

app.use('/', index);
app.use('/users', users);
app.use('/authenticate', authenticate);
app.use('/contents',contents);
app.use('/image',image);
app.use('/category',cats);
app.use('/report',reports);
app.use('/tags',tags);
app.use('/partners',partners);
app.use('/contents',special_hacks);
app.use('/admin',admin);
app.use('/web',webview);
app.use('/notify',notify);
app.use('/',legacy);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


// Mongoose connect
mongoose.connect(Config.database);

module.exports = app;
port = process.env.PORT || Config.port;

app.listen(port);
